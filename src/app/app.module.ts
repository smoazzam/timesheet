import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeeService } from './services/employee.service';
import { TimesheetComponent } from './timesheet/timesheet.component';
import { TaskService } from './services/task.service';
import { TimesheetService } from './services/timesheet.service';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    TimesheetComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: '', component: EmployeeListComponent},
      { path: 'employees', component: EmployeeListComponent},
      { path: 'timesheet/:empId', component: TimesheetComponent},
      { path: '**', redirectTo: '/employees', pathMatch: 'full'},
  ]),
  ],
  providers: [
    EmployeeService,
    TaskService,
    TimesheetService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
