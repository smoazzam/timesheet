import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Utility } from '../utility';

@Component({
    selector: 'employee-list',
    templateUrl: 'employee.component.html',
    styleUrls:['./employee.component.scss']
})

export class EmployeeListComponent implements OnInit {
    employees: any;
    constructor(private employeeService: EmployeeService) { }

    ngOnInit() {
        let dates = Utility.getDateRangeOfWeek(Utility.getNumberOfWeek(new Date()));
        this.employeeService.getallemployeeswitheffort(dates[0], dates[6]).subscribe(data => {
            this.employees = data;
        });
    }    
}