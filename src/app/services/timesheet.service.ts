import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TimesheetService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    gettimesheet(employeeid, startdate, enddate) {
        return this.http.get(this.baseapi + `/timesheet/get?employeeid=${employeeid}&startdate=${startdate}&enddate=${enddate}`);
    }

    savetimesheet(timesheet) {
        return this.http.post(this.baseapi + `/timesheet/save`, timesheet);
    }
}