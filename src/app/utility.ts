export class Utility {
    static monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"];
    
    static dayNames: any = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    
    static getDateRangeOfWeek(weekNo: number) {
        let d1 = new Date();
        let numOfdaysPastSinceLastSunday = d1.getDay();
        d1.setDate(d1.getDate() - numOfdaysPastSinceLastSunday);
        let weekNoToday = Utility.getNumberOfWeek(d1);
        let weeksInTheFuture = weekNo - weekNoToday;
        d1.setDate(d1.getDate() + (7 * weeksInTheFuture));
        let dayOne = (d1.getMonth() + 1) + "/" + d1.getDate() + "/" + d1.getFullYear();
    
        let dates = [];
        dates.push(dayOne);
    
        for (let i = 1; i <= 6; i++) {
          d1.setDate(d1.getDate() + 1);
          let day = (d1.getMonth() + 1) + "/" + d1.getDate() + "/" + d1.getFullYear();
          dates.push(day);
        }
        return dates;
      }
    
      static getNumberOfWeek(date) {
        const currentDay = new Date(date);
        const firstDayOfYear = new Date(currentDay.getFullYear(), 0, 1);
        const pastDaysOfYear = (currentDay.valueOf() - firstDayOfYear.valueOf()) / 86400000;
        return Math.ceil((pastDaysOfYear + firstDayOfYear.getDay() + 1) / 7);
      }
    
}