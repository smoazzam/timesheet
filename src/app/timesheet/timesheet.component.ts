import { Component, OnInit } from '@angular/core';
import { TaskService } from '../services/task.service';
import { EmployeeService } from '../services/employee.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TimesheetService } from '../services/timesheet.service';
import { Utility } from '../utility';

@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.scss']
})
export class TimesheetComponent implements OnInit {

  weekHeader: any = [];

  currentEmployee: any;
  employees: any = [];
  tasks: any = [];
  employeetasks: any = [];

  currentWeek: number = -1;
  currentWeekStartDate: any;
  currentWeekEndDate: any;
  currentMonthYear: any;
  msg: Message;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private taskService: TaskService,
    private employeeService: EmployeeService,
    private timesheetService: TimesheetService) { }

  ngOnInit() {
    this.currentEmployee = +this.route.snapshot.params.empId;
    this.currentWeek = Utility.getNumberOfWeek(new Date());
    this.initialize();
  }

  initialize() {
    this.msg = {isError: false, message: ''};
    this.initializeWeekHeader();
    this.loadEmployees();
    this.loadTasks();
    this.loadEmployeeTimesheet();
  }

  initializeWeekHeader() {
    let dates = Utility.getDateRangeOfWeek(this.currentWeek);
    this.initializeCurrentWeekCaption(new Date(dates[0]), new Date(dates[6]));
    this.currentWeekStartDate = dates[0];
    this.currentWeekEndDate = dates[6];
    this.weekHeader = [];
    dates.forEach((date,index) => {
      this.weekHeader.push({date: date, day: Utility.dayNames[index]}); 
    });  
  }

  initializeCurrentWeekCaption(startdate, enddate){
    this.currentMonthYear = (startdate.getMonth() == enddate.getMonth()) ? 
    `${Utility.monthNames[startdate.getMonth()]} ${startdate.getFullYear()}` : 
    `${Utility.monthNames[startdate.getMonth()]} - ${Utility.monthNames[enddate.getMonth()]}  ${startdate.getFullYear()}`;
  }

  employeeOnChange() {
    this.router.navigateByUrl(`/timesheet/${this.currentEmployee}`);
    this.initialize();
  }

  loadEmployees() {
    this.employeeService.getallemployees().subscribe(data => {
      this.employees = data;
    });
  }

  loadTasks() {
    this.taskService.getalltasks().subscribe(data => {
      this.tasks = data;
    });
  }

  loadEmployeeTimesheet() {
    this.timesheetService.gettimesheet(this.currentEmployee, this.currentWeekStartDate, this.currentWeekEndDate).subscribe(data => {
      this.employeetasks = data;
    });
  }

  totalHours(day) {
    return this.employeetasks.reduce((a, b) => a + (+b.days[day].hoursWorked || 0), 0);
  }

  changeWeek(weeknumber){
    this.currentWeek = weeknumber;
    this.initialize();
  }

  addNewRecord() {
    this.msg = {isError: false, message: ''};
    let task: TaskLog = new TaskLog();
    task.taskid = 0;
    this.weekHeader.forEach(item => {
      task.days.push({date: item.date, hoursWorked: 0});
    });
    this.employeetasks.push(task);
  }

  saveTimesheet() {
    let worklogs: any = [];
    this.employeetasks.forEach(element => {
      element.days.forEach(day => {
        worklogs.push({
          "employeeid": this.currentEmployee,
          "taskid": element.taskid,
          "taskdate": day.date,
          "taskhours": +day.hoursWorked
        })
      });
    });

    let timesheet = { 
      "employeeid": this.currentEmployee,
      "startdate": this.weekHeader[0].date,
      "enddate": this.weekHeader[6].date,
      "worklogs": worklogs,
   }
   
    if (worklogs.length == 0) {
      this.msg = { isError: true, message: 'Message: Please add work/task item(s)!' };
      return;
    }

    this.timesheetService.savetimesheet(timesheet).subscribe(response => {
      this.msg = {isError: false, message: 'Message: employee timsheet saved successfully!'};
    },
    error => {
      this.msg = {isError: true, message: 'Message: something went wrong!'};
    });
  }

}

export class TaskLog {
  taskid: number = 0;
  days: WorkDays[] = [];
}

export class WorkDays {
  date: Date;
  hoursWorked: number = 0;
}

export class Message{
  message: string;
  isError: boolean = false;
}